import { Event, Messages } from '../types';

export const interHackMessages: Messages = {
  'pt-BR': {
    title: 'Campeonato InterHack',
    date: 'Junho e Novembro de 2019',
    local: 'IME - USP, ICMC - USP, EACH - USP',
    description: `O Campeonato InterHack é o evento principal de 2019. Consiste em 4 hackathons:
    3 eliminatórias regionais no 1º semestre e a grande final, em novembro. As elimininatórias ocorrerão
    simultâneamente nos campi Butantã (IME-USP), São Carlos (ICMC-USP) e Leste (EACH), no 1º semestre.
    De cada eliminatória, serão selecionadas 5 equipes para competir na final em novembro. A final irá ocorrer
    no campus Butantã, podendo valer uma viagem com duração de 1 semana à um grande polo tecnológico,
    nacional ou internacional. Também, em Julho, faremos o HackerCamp, um treinamento especial
    com os finalistas, onde nossos maiores patrocinadores terão acesso a alguns dos programadores da USP.`,
  },
  'en-US': {
    title: 'InterHack Championship',
    date: 'June and November 2019',
    local: 'IME - USP, ICMC - USP, EACH - USP',
    description: `The InterHack Championship is the main event of 2019. It will consist of 4 hackathons:
    3 regional selective in the 1st semester and the big final, in November. The first round of hackathons will happen
    simultaneously at the Campi Butantã (IME-USP), São Carlos (ICMC-USP) e Leste (EACH), in the 1st semester.
    From each selective, it will be selected 5 teams to compete in the final event in November. The final will be hosted
    at the campus Butantã, it may offer a one week trip to a big technological pole, national or international. Also, in July, 
    we will do the HackerCamp, special training for the finalists, where our top sponsors will have access to some
    of the best programmers at USP. `,
  },
};

const interhack: Event = {
  id: 'interhack',
  participants: 180,
  color: 'orange',
  linearGradient: 'linear-gradient(180deg, #FF690A 0%, #B74F0E 100%)',
  quotas: [
    {
      name: 'Bronze',
      prices: {
        'pt-BR': 1500,
        'en-US': 2250,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento ao patrocinador em todas as atividades relacionadas ao evento no campi de São Carlos.`,
          'en-US': ``,
        },
        {
          'pt-BR': `Oportunidade de trazer visitantes ao evento de São Carlos.`,
          'en-US': `Possibility to offer awards to the competing teams`,
        },
        {
          'pt-BR': `Possibilidade de distribuir folders, brindes e outros materiais de São Carlos`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },
        {
          'pt-BR': `Oportunidade de exibir banner do patrocinador no evento de São Carlos`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },

      ],
    },
    {
      name: 'Silver',
      prices: {
        'pt-BR': 3000,
        'en-US': 3000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Bronze`,
          'en-US': `All benefits from Bronze`,
        },
        {
          'pt-BR': `Possibilidade de ter um representante do patrocinador entre
          os mentores do evento`,
          'en-US': `Possibility to have a sponsor's representative between the mentors
          of the event`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 15 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to 15 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Gold',
      prices: {
        'pt-BR': 5000,
        'en-US': 4500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Silver`,
          'en-US': `All benefits from Silver`,
        },
        {
          'pt-BR': `Possibilidade de acesso aos emails dos participantes do evento
          que consentiram tal acesso`,
          'en-US': `Possibility to access the emails of the participants of the event
          that allowed it`,
        },
        {
          'pt-BR': `Possibilidade de apontar um representante do patrocinador
          como juíz do evento`,
          'en-US': `Possibility to point a sponsor's representative as a judge of
          the event`,
        },
      ],
    },
  ],
};
export default interhack;
