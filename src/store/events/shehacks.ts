import { Event, Messages } from '../types';

export const sheHacksMessages: Messages = {
  'pt-BR': {
    title: 'SheHacks',
    date: 'Setembro de 2019',
    local: 'empresa parceira',
    description: `SheHacks é o nosso evento que visa aumentar a participação feminina na
    comunidade hacker brasileira. O evento é um hackday, hackathon com duração
    de 10 horas ao invés de 24, as participantes apresentaram soluções para trazer visibilidade
    às conquistas obtidas por mulheres.`,
  },
  'en-US': {
    title: 'SheHacks',
    date: 'September 2019',
    local: 'partner company',
    description: `SheHacks is our event which aims increase female participation in the
    Brazilian hacker community. The event is a hackday, a 10 hour hackathon, instead of 24 hours,
    the teams will present solutions to make women accomplishments visible.`,
  },
};

const shehacks: Event = {
  id: 'shehacks',
  participants: 40,
  color: 'red',
  linearGradient: 'linear-gradient(180deg, #ef5753 0%, #cc1f1a 100%)',
  quotas: [
    {
      name: 'Bronze',
      prices: {
        'pt-BR': 1500,
        'en-US': 2250,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento ao patrocinador em todas as atividades relacionadas ao evento no campi de São Carlos.`,
          'en-US': ``,
        },
        {
          'pt-BR': `Oportunidade de trazer visitantes ao evento de São Carlos.`,
          'en-US': `Possibility to offer awards to the competing teams`,
        },
        {
          'pt-BR': `Possibilidade de distribuir folders, brindes e outros materiais de São Carlos`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },
        {
          'pt-BR': `Oportunidade de exibir banner do patrocinador no evento de São Carlos`,
          'en-US': `Possibility to make a up to a 5 minutes presentation about the
          sponsor in the opening of the event`,
        },

      ],
    },
    {
      name: 'Silver',
      prices: {
        'pt-BR': 3000,
        'en-US': 3000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Bronze`,
          'en-US': `All benefits from Bronze`,
        },
        {
          'pt-BR': `Possibilidade de ter um representante do patrocinador entre
          os mentores do evento`,
          'en-US': `Possibility to have a sponsor's representative between the mentors
          of the event`,
        },
        {
          'pt-BR': `Possibilidade de apresentação de até 15 minutos do patrocinador
          na abertura do evento`,
          'en-US': `Possibility to make a up to 15 minutes presentation about the
          sponsor in the opening of the event`,
        },
      ],
    },
    {
      name: 'Gold',
      prices: {
        'pt-BR': 5000,
        'en-US': 4500,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Silver`,
          'en-US': `All benefits from Silver`,
        },
        {
          'pt-BR': `Possibilidade de acesso aos emails dos participantes do evento
          que consentiram tal acesso`,
          'en-US': `Possibility to access the emails of the participants of the event
          that allowed it`,
        },
        {
          'pt-BR': `Possibilidade de apontar um representante do patrocinador
          como juíz do evento`,
          'en-US': `Possibility to point a sponsor's representative as a judge of
          the event`,
        },
      ],
    },
  ],
};

export default shehacks;
