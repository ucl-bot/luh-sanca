import { Event, Messages } from '../types';

export const allMessages: Messages = {
  'pt-BR': {
    title: 'Patrocione tudo!',
    date: '',
    local: '',
    description: `Se você gostou de todos nossos eventos, pode patrocinar
    todos nossos eventos com 10% de desconto em relação a soma do preço da cota Gold
    de todos os eventos. Mas se você quer exclusividade dentro do seu seguimento,
    a cota Adamantium é o que você procura.`,
  },
  'en-US': {
    title: 'Sponsor everything!',
    date: '',
    local: '',
    description: `If you enjoyed all our events, you can sponsor all of them at the same time
    with a 10% discount in relation to the totla sum of the Gold quota. However, if you seek
    exclusivity in your segment, the Adamantium quota is intended for your needs`,
  },
};

const global: Event = {
  id: 'all',
  participants: 0,
  linearGradient: '',
  color: 'yellow-dark',
  quotas: [
    {
      name: 'Adamantium',
      prices: {
        'pt-BR': 80000,
        'en-US': 24000,
      },
      benefitsList: [
        {
          'pt-BR': `Agradecimento e reconhecimento especial do patrocinador em
          todas as atividades relacionadas ao evento.`,
          'en-US': `Thanks and recognition to the sponsor in all activities releated
          to the event`,
        },
        {
          'pt-BR': `Todos os benefícios da cota Gold em todos nossos eventos.`,
          'en-US': `All the benefits from Gold quotas in all our events`,
        },
      ],
    },
    {
      name: 'Vibranium',
      prices: {
        'pt-BR': 120000,
        'en-US': 36000,
      },
      benefitsList: [
        {
          'pt-BR': `Todos os benefícios da Adamantium`,
          'en-US': `All benefits from Adamantium`,
        },
        {
          'pt-BR': `Garantia de exclusividade como empresa de seu segmento.`,
          'en-US': `Guarantee of exclusivity as a company of your segment`,
        },
      ],
    },
  ],
};

export default global;
