import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';

import { Event, RootState } from './types';

import aghack from '@/store/events/aghack';
import all from '@/store/events/all';
import deephack from '@/store/events/deephack';
import interhack from '@/store/events/interhack';
import shehacks from '@/store/events/shehacks';

Vue.use(Vuex);

const isStartupConvertion = (): boolean => {
  const isStartup = localStorage.getItem('isStartup');
  if (!isStartup || isStartup === 'false') {
    return false;
  }

  return true;
};

const store: StoreOptions<RootState> = {
  state: {
    eventList: [
      interhack,
      deephack,
      shehacks,
      aghack,
      all,
    ],
    selectedHackathon: interhack,
    isStartup: isStartupConvertion(),
  },

  getters: {
    getEventList(state: RootState): Event[] {
      return state.eventList.filter((item: Event) => {
        return item.id !== 'all';
      });
    },

    selectedHackathon(state: RootState): Event {
      return state.selectedHackathon;
    },

    isStartup(state: RootState) {
      return state.isStartup;
    },
  },

  mutations: {
    selectEvent(state: RootState, id: string) {
      const event = state.eventList.find((item: Event) => {
        return id === item.id;
      });

      if (!event) {
        throw Error('Event not found');
      }

      state.selectedHackathon = event;
    },

    selectPricingOption(state: RootState, option: boolean) {
      localStorage.setItem('isStartup', `${option}`);
      state.isStartup = option;
    },
  },
};

export default new Vuex.Store<RootState>(store);
