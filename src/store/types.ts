export interface RootState {
  eventList: Event[];
  selectedHackathon: Event;
  isStartup: boolean;
}

interface BenefitMessage {
  'pt-BR': string;
  'en-US': string;
}

export interface Quota {
  name: string;
  prices: {
    'pt-BR': number;
    'en-US': number;
  };
  benefitsList: BenefitMessage[];
}

interface HackathonCharacteristics {
  title: string;
  date: string;
  local: string;
  description: string;
}

export interface Messages {
  'pt-BR': HackathonCharacteristics;
  'en-US': HackathonCharacteristics;
}

export interface Event {
  id: string;
  participants: number;
  color: string;
  linearGradient: string;
  quotas: Quota[];
}
