import Vue from 'vue';
import VueI18n from 'vue-i18n';

import { agHackMessages } from '@/store/events/aghack';
import { allMessages } from '@/store/events/all';
import { deepHackMessages } from '@/store/events/deephack';
import { interHackMessages } from '@/store/events/interhack';
import { sheHacksMessages } from '@/store/events/shehacks';

const numberFormats: any = {
  'pt-BR': {
    currency: {
      style: 'currency',
      currency: 'BRL',
    },
  },
  'en-US': {
    currency: {
      style: 'currency',
      currency: 'USD',
    },
  },
};

const messages: any = {
  'pt-BR': {
    global: {
      language: 'Português',
      description: `
        Um <em>hackathon</em> do <span class="text-orange">USPCodeLab</span> é uma competição de
        programação, onde as equipes participantes passam 24 horas
        criando uma solução de software / hardware para o tema do evento.
        A L.U.H. será a liga que centralizará todos os eventos de <em> hacking </em>
        organizados pelo <span class="text-orange">USPCodeLab</span>, escolha os eventos que quer
        patrocinar e fale conosco para viabilizar nossa parceria:`,
      sponsor: 'Patrocine!',
      sponsorAll: 'Patrocine Tudo!!',
      participants: 'Participantes',
      date: 'Data',
      local: 'Local',
    },
    interhack: interHackMessages['pt-BR'],
    deephack: deepHackMessages['pt-BR'],
    shehacks: sheHacksMessages['pt-BR'],
    aghack: agHackMessages['pt-BR'],
    all: allMessages['pt-BR'],
  },
  'en-US': {
    global: {
      language: 'English',
      description: `
        For <span class="text-orange">USPCodeLab</span> a <em>hackathon</em> is a programming competition,
        in which the participating teams spend 24 hours creating a software / hardware solutin for
        the event's theme. The L.U.H. is the centralizing league of all <em> hacking </em>
        events organized by <span class="text-orange">USPCodeLab</span>, pick the events you want
        to sponsor and talk to us to make our parternship became a reality:`,
      sponsor: 'Sponsor Us!',
      sponsorAll: 'Sponsor Everything!!',
      participants: 'Participants',
      date: 'Date',
      local: 'Local',
    },
    interhack: interHackMessages['en-US'],
    deephack: deepHackMessages['en-US'],
    shehacks: sheHacksMessages['en-US'],
    aghack: agHackMessages['en-US'],
    all: allMessages['en-US'],
  },
};

Vue.use(VueI18n);

export default new VueI18n({
  locale: 'pt-BR',
  fallbackLocale: 'en-US',
  messages,
  numberFormats,
});
